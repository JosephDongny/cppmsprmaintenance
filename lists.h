#ifndef LISTS_H_
#define LISTS_H_

#include <string>
#include <vector>
#include <list>
#include <queue>
#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <algorithm>

#include "util.h"

enum struct Categories
{ 
    Rock, Pop, Rap, Techno, Sports, Science, Philosophy, Litterature, Geography, People, Null
};


inline std::string categories_to_string(Categories cat)
{
        switch(cat)
        {
            case Categories::Rock:
                return "Rock";
            case Categories::Pop:
                return "Pop";
            case Categories::Rap:
                return "Rap";
            case Categories::Techno:
                return "Techno";
            case Categories::Science:
                return "Science";
            case Categories::Sports:
                return "Sports";
            case Categories::Philosophy:
                return "Philosophy";
            case Categories::Litterature:
                return "Litterature";
            case Categories::Geography:
                return "Geography";
            case Categories::People:
                return "People";
            default: 
                return "";
        }
}

inline Categories getCategorieFromString(std::string cat)
{
        if(cat == "Rock")
        {
            return Categories::Rock;
        }
        if(cat == "Science")
        {
            return Categories::Science;
        }
        if(cat == "Sports")
        {
            return Categories::Sports;
        }
        if(cat == "Pop")
        {
            return Categories::Pop;
        }
        if(cat == "Techno")
        {
            return Categories::Techno;
        }
        if(cat == "Rap")
        {
            return Categories::Rap;
        }
        if(cat == "Philosophy")
        {
            return Categories::Philosophy;
        }
        if(cat == "Litterature")
        {
            return Categories::Litterature;
        }
        if(cat == "Geography")
        {
            return Categories::Geography;
        }
        if(cat == "People")
        {
            return Categories::People;
        }

        return Categories::Null;
}

static const Categories All[] = { Categories::Rock, Categories::Pop, Categories::Techno, Categories::Sports, Categories::Science, Categories::Rap, Categories::Philosophy, Categories::Litterature, Categories::Geography, Categories::People};


struct PhraseQuestion
{
    std::string _valeur;

    PhraseQuestion()
    {
        _valeur = "question";
        //_valeur = util::random_name(12) + " ?";
    }

    PhraseQuestion(std::string phrase)
    {
        _valeur = phrase;
    }
};

struct Question
{
    PhraseQuestion _phrase;

    Question(std::string phrase)
    {
        _phrase = PhraseQuestion(phrase);
    }

    Question(std::string phrase, Categories cat)
    {
        phrase += categories_to_string(cat);
        _phrase = PhraseQuestion(phrase);
    }

    Question(const PhraseQuestion& phrase)
    {
        _phrase = phrase;
    }

    std::string toString()
    {
        return _phrase._valeur;
    }

    void ask()
    {
        std::cout << _phrase._valeur << std::endl;
    }

};

struct ListeDeQuestions
{
    std::queue<Question> _questions;

    ListeDeQuestions()
    {
    }

    void ajouterQuestion(const Question& question)
    {
        _questions.push(question);
        std::cout<<"added question, size: "<<_questions.size()<<std::endl;
    }

    void askQuestion()
    {
        if(_questions.size() > 0)
        {
            _questions.front().ask();
            return;
        }
        //std::cout<<"plus de questions disponibles pour ce theme"<<std::endl;  

        Question qu = Question(util::random_name(10));
        ajouterQuestion(qu); 
    }

    Question front()
    {
        return _questions.front();
    }

    void pop()
    {
        _questions.pop();
    }


};

struct DeckDeQuestions
{
    std::unordered_map<Categories, ListeDeQuestions> _deck;
    bool _rock;

    DeckDeQuestions()
    {
        _rock = true;
        
        for (const auto cat : All) 
	    {
            ListeDeQuestions lq = ListeDeQuestions();
            _deck.insert({cat, lq});
        }
    }

    DeckDeQuestions(bool rock)
    {
        _rock = rock;
        
        for (const auto cat : All) 
	    {
            ListeDeQuestions lq = ListeDeQuestions();
            _deck.insert({cat, lq});
        }
    }

    void ajouterQuestion(Categories categorie, const Question& question)
    {
        _deck[categorie].ajouterQuestion(question);
    }

    void refillDeck()
    {
        for (const auto cat : All) 
	    {
            this->populateQuestions(cat,5);
        }
    }

    void populateQuestions(Categories categorie, int iteration_number)
    {
        for(int i = 0; i < iteration_number; i++)
        {
            std::cout<<"adding question"<<std::endl;
            Question question = Question("Question "+std::to_string(i), categorie);
            _deck[categorie].ajouterQuestion(question);
        }
    }

    void removeListeQuestion(Categories cat)
    {
        _deck.erase(cat);
    }

    ListeDeQuestions operator[](Categories cat)
    {
        return _deck[cat];
    }
};

struct Joueur
{

        std::string _nom;

        bool _joker = true;
        int _purse = 0;
        int _currentplace = 0;
        bool _penalty = false;
        int _timesInPenalty = 0;
        int _inPrisonSince = 0;

        int _winstreak = 0;


    Joueur()
    {
        _nom = util::random_name(6);
    }

    Joueur(std::string nom)
    {
        _nom = nom;
    }

    void moveForward(int roll)
    {
        _currentplace += roll;
        if(_currentplace > 11)
        {
            _currentplace = 0;
        }
    }

    void tryToGetOutOfPenalty()
    {
        std::cout<<std::to_string(_timesInPenalty)<<std::endl;
        if(_timesInPenalty <= 0)
        {
            return;
        }

        int random = rand()%100+1;
        int luck = (1/_timesInPenalty) * 100 + 10 * _inPrisonSince;
        std::cout<<std::to_string(luck)<<"% de chance de sortir ("<<std::to_string((1/_timesInPenalty) * 100)<<"% + "<<std::to_string(10 * _inPrisonSince)<<"%)"<<std::endl;
        
        if(luck < random)
        {
           setPenalty(false);
           std::cout << "You are lucky ! You can go out of the penalty box !";
        }
    }

    bool increaseGold(int _goldNeededToWin)
    {
        this->_purse = this->_purse + 1;

        if(this->_winstreak >= 2)
        {
            this->_purse = this->_purse + this->_winstreak - 1;
        }

        if(this->_purse >= _goldNeededToWin)
        {
            std::cout << std::endl;
            std::cout << this->_nom << " a fini !"<<std::endl;
            
            return true;
        }
        return false;
    }

    void increaseWinstreak()
    {
        this->_winstreak++;
    }

    void loseWinstreak()
    {
        this->_winstreak = 0;
    }

    void useJoker()
    {
        this->_joker = false;
    }

    bool hasJoker()
    {
        return this->_joker;
    }

    bool estEnPenalty()
    {
        return this->_penalty;
    }

    void setPenalty(bool penalty)
    {
        this->_penalty = penalty;

        if (penalty == false)
        {

            this->resetInPrisonSince();
        }
    }

    void increaseTimeInPenalty()
    {
        this->_timesInPenalty = this->_timesInPenalty + 1;
    }

    void increaseInPrisonSince()
    {
        this->_inPrisonSince = this->_inPrisonSince + 1;
    }

    void resetInPrisonSince()
    {
        this->_inPrisonSince = 0;
    }

    void reset()
    {
        this->_joker = true;
        this->_purse = 0;
        this->_currentplace = 0;
        this->_penalty = false;
        this->_timesInPenalty = 0;
        this->_inPrisonSince = 0;
        this->_winstreak = 0;
    }

    bool operator==(const Joueur& other)
    {
        return _nom == other._nom;
    }
};


struct ListeJoueurs
{
    std::vector<Joueur *> _joueurs;
    size_t _current = 0;

    void ajouterJoueur(Joueur* joueur)
    {
        _joueurs.push_back(joueur);
    }

    void reset()
    {
        for(size_t i=0; i<_joueurs.size();i++)
        {
            std::string nom = _joueurs[i]->_nom;
            _joueurs[i] = new Joueur(nom);
        }
    }

    void retirerJoueur(Joueur* joueur)
    {
        auto it = find(_joueurs.begin(), _joueurs.end(), joueur);
 
        if (it != _joueurs.end()) 
        {
            int index = it - _joueurs.begin();
            _joueurs.erase(_joueurs.begin() + index);
        }
    }

    bool bonNombreDeJoueurs()
    {
        if(_joueurs.size() > 6 || _joueurs.size() < 2)
        {
            return false;
        }

        return true;
    }

    bool AtLeastThreePlayers(){

        if(_joueurs.size() < 2)
        {
            return false;
        }else
        {
            return true;
        }

    }

    size_t size()
    {
        return _joueurs.size();
    }

    void incrementCurrentPlayer()
    {
        if(_current < _joueurs.size() - 1)
        {
            _current ++ ;
            std::cout<< _joueurs[_current]->_nom << "'s TURN"<<std::endl;
            return;
        }

        _current = 0;
        std::cout<<std::endl;
        std::cout<< _joueurs[_current]->_nom << "'s TURN"<<std::endl;
    }

    Joueur* currentPlayer()
    {
        std::vector <Joueur*>::iterator it = _joueurs.begin() + _current;
        return *it;
    }
};

#endif // LISTS_H_