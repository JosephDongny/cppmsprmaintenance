#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <algorithm>

#include "lists.h"

namespace util 
{
    inline std::string random_name( size_t length )
    {
        auto randchar = []() -> char
        {
            const char charset[] =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";
            const size_t max_index = (sizeof(charset) - 1);
            return charset[ rand() % max_index ];
        };
        std::string str(length,0);
        std::generate_n( str.begin(), length, randchar );
        return str;
    }
}

#endif // UTIL_H