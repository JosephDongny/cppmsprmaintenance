﻿#include <stdlib.h>
#include <time.h>
#include "Game.h"

static bool notAWinner;



void play(Game game)
{
	
	if(!game.isPlayable())
	{
		std::cout<<"Game is not playable"<<std::endl;
		return;
	}

	std::string joker;
	bool use_joker;
	do
	{
		game.roll(rand() % 5 + 1);
		use_joker=false;

		if (rand() % 9 == 7)
		{
			
			if(game.getCurrentPlayerJoker())
			{
				std::cout<<"Voulez vous utiliser votre joker ? Oui / Non ";
				do
				{
					std::cin >> joker;
					std::cout<<joker<<std::endl;
				}
				while((joker != "Oui") && (joker !="Non"));
			}
			if(joker == "Oui")
			{
				use_joker=true;
			}
			if(use_joker)
			{
				game.useJoker();
				notAWinner = game.answeredWithJoker();
				use_joker=false;
			}
			else
			{
				notAWinner = game.wrongAnswer();
			}
			
		}
		else
		{
			notAWinner = game.wasCorrectlyAnswered();
			if(game._over)
			{
				std::cout<<"GAME OVER"<<std::endl;
				return;
			}

		}

	} while(notAWinner);
}

int main()
{
	srand(time(NULL));

	Game aGame;
	
	aGame.add("Gregz");
	aGame.add("Romainz");
	aGame.add("Florianz");
	aGame.add("Joseph");
	aGame.add("Florentz");

	std::string val;
	std::cout<<"Voulez vous commencer avec une question Techno ? Oui / Non "<<std::endl;
	do
	{
		std::cin >> val;
	}
	while((val != "Oui") && (val !="Non"));

	aGame.Techno(val == "Oui");

	Joueur joueur = Joueur("Chet");

	//aGame.leaveGame(&joueur);

	int gold;
	//sdt::string val2;
	std::cout << "Combien de golds pour gagner ? Entrez une valeur entre 6 et 20 "<<std::endl;
	do
	{
		std::cin >> gold;

		std::cout<<gold<<std::endl;
	}
	while((gold < 6) || (gold > 200));

	aGame.ChangeGoldNeededToWin(gold);

	play(aGame);

	std::string response;
	std::cout<<"Voulez vous Refaire cette partie ? Oui/Non "<<std::endl;
	std::cin >> response;

	while(response == "Oui")
	{
		aGame.reset();
		play(aGame);
		std::cout<<"Voulez vous Refaire cette partie ? Oui/Non "<<std::endl;
		std::cin >> response;
	}

	return 0;
}
