#include "Game.h"
#include "lists.h"


bool AtLeastTwoAndMaxSixPlayers()
{
    Game aGame;

    aGame.add("j1");
	aGame.add("j2");
	aGame.add("j3");
	aGame.add("j4");
	aGame.add("j5");


    if (aGame._joueurs.size() > 6 && aGame._joueurs.size() < 2) return false;


    return true;
}
