#ifndef GAME_H_
#define GAME_H_

#include "lists.h"
#include <iostream>
#include <list>
#include <vector>


class Game
{
	private:
		ListeJoueurs _joueurs;

		DeckDeQuestions _deck;

		Categories _nextCategorie = Categories::Null;

		int _goldNeededToWin = 6;
		int _limiteLeaderboard = 3;

		void askQuestion();
		Categories _currentCategory;
		Categories currentCategory();
			
		bool _techno;

		std::vector<Joueur> ArrayWinner;

	public:
		bool _over = false;

		Game();
		Game(const Game& other);

		void reset();
		bool getCurrentPlayerJoker();
		Joueur* getCurrentPlayer();
		void ChangeNextCategory(Categories cat);
		void ChangeGoldNeededToWin(int gold);
		void Techno(bool techno);
		void leaveGame(Joueur*);
		bool isPlayable();
		bool add(std::string playerName);

		bool useJoker();

		Question createQuestion(Categories);
		
		void roll(int roll);
		bool wasCorrectlyAnswered();
		bool answeredWithJoker();
		bool wrongAnswer();

		bool AtLeastThreePlayers();

		

};

#endif // GAME_H_
