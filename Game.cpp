﻿#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>

#include "Game.h"
#include "lists.h"
#include "util.h"


Game::Game()
{
	_deck = DeckDeQuestions();

	for (const auto cat : All) 
	{
		_deck.populateQuestions(cat, 1);
	}
}

Game::Game(const Game& other)
{
	_deck = other._deck;
	_joueurs = other._joueurs;
	_goldNeededToWin = other._goldNeededToWin;
	_techno = other._techno;

	for (const auto cat : All) 
	{
		_deck.populateQuestions(cat, 1);
	}
}

Question Game::createQuestion(Categories cat)
{
	Question question = Question("Question " , cat);
	
	return question;
}

bool Game::isPlayable()
{
	return _joueurs.bonNombreDeJoueurs();
}

bool Game::add(std::string playerName)
{
	Joueur* joueur = new Joueur(playerName);
	_joueurs.ajouterJoueur(joueur);

	std::cout << playerName << " was added" << std::endl;
	return true;
}


void Game::Techno(bool techno)
{
	_techno = techno;
	if(techno)
	{
		_deck.removeListeQuestion(Categories::Rock);
		return;
	}

	_deck.removeListeQuestion(Categories::Techno);
}

void Game::leaveGame(Joueur* joueur)
{
	_joueurs.retirerJoueur(joueur);
	
	std::cout<<"Un joueur quitte, il y a maintenant " << std::to_string(_joueurs.size()) << " joueurs dans la partie." <<std::endl;

	if(!isPlayable())
	{
		std::cout<<"Plus assez de joueurs, fin de la partie!"<<std::endl;
		exit(0);		
	}
}

void Game::roll(int roll)
{
	Joueur* joueur = _joueurs.currentPlayer();
	std::cout << joueur->_nom << " is the current player" << std::endl;
	std::cout << "They have rolled a " << roll << std::endl;

	_currentCategory = currentCategory();
	if (joueur->estEnPenalty())
	{
		std::cout << "They are trying to get out of penalty!" << std::endl;
		joueur->tryToGetOutOfPenalty();
		std::cout << joueur->_nom << "  WAS IN JAIL" << std::endl;
		if (roll % 2 != 0)
		{
			std::cout << joueur->_nom  << " IS FREE !" << std::endl;
			joueur->setPenalty(false);
		}
		else
		{
			std::cout << joueur->_nom << "  ne sort pas de la penalty box" << std::endl;
			_joueurs.incrementCurrentPlayer();
			return;
		}
	}

	joueur->moveForward(roll);

	std::cout << joueur->_nom << "'s new location is " << std::to_string(joueur->_currentplace) << std::endl;
	std::cout << "The category is " << categories_to_string(_currentCategory) << std::endl;
	askQuestion();

	_joueurs.incrementCurrentPlayer();
}

void Game::askQuestion()
{
	std::cout << categories_to_string(_currentCategory) << std::endl;
	std::cout << _deck[_currentCategory].front().toString() << std::endl;
	_deck[_currentCategory].pop();
}

void Game::ChangeNextCategory(Categories cat)
{
	_nextCategorie = cat;
}

Categories Game::currentCategory()
{	
	Joueur* joueur = _joueurs.currentPlayer();
	
	if(_nextCategorie!=Categories::Null)
	{
		Categories tmp = _nextCategorie;
		std::cout << "The next category is " << categories_to_string(tmp) << std::endl;
		_nextCategorie = Categories::Null;
		return tmp;
	}else
	{
		if(_techno){
		switch(joueur->_currentplace) 
		{
			case 0: 
			case 4: return Categories::Geography; break;
			case 8: return Categories::Pop ; break;
			case 1: return Categories::Rap; break;
			case 5: return Categories::People; break;
			case 9: return Categories::Science; break;
			case 2: return Categories::Philosophy; break;
			case 6: return Categories::Litterature; break;
			case 10: return Categories::Sports; break;
			default : return Categories::Techno; break;
		}
		}
		else{
			switch(joueur->_currentplace) 
			{
				case 0: 
				case 4: return Categories::Geography; break;
				case 8: return Categories::Pop ; break;
				case 1: return Categories::Rap; break;
				case 5: return Categories::People; break;
				case 9: return Categories::Science; break;
				case 2: return Categories::Philosophy; break;
				case 6: return Categories::Litterature; break;
				case 10: return Categories::Sports; break;
				default : return Categories::Rock; break;
			}
		}
	}
	
	
}

bool Game::wasCorrectlyAnswered()
{
	Joueur* joueur = _joueurs.currentPlayer();
	std::cout << "Answer was correct!!!!" << std::endl;

	if (joueur->estEnPenalty())
	{	
		joueur->setPenalty(false);
	}
	else
	{
		joueur->increaseWinstreak();
	}
	
	bool winner = joueur->increaseGold(_goldNeededToWin);
	
	std::cout << joueur->_nom
		      << " now has "
			  << joueur->_purse
		      <<  " Gold Coins." << std::endl;

	if(winner){

		_limiteLeaderboard--;
        ArrayWinner.push_back(*joueur);
		_joueurs.retirerJoueur(joueur);

            if(_limiteLeaderboard<=0 || !_joueurs.bonNombreDeJoueurs())
			{
				for(size_t i = 0; i<ArrayWinner.size(); i++)
				{
					std::cout<<std::to_string(i+1)<<": "<<ArrayWinner[i]._nom<<", bravo !"<<std::endl;
				}
				_over = true;
			}
	}
	

	_joueurs.incrementCurrentPlayer();
	return true;
}

bool Game::answeredWithJoker()
{
	Joueur* joueur = _joueurs.currentPlayer();
	std::cout << "Joker used!!!!" << std::endl;

	if (!joueur->estEnPenalty())
	{	
		joueur->setPenalty(false);
	}

	_joueurs.incrementCurrentPlayer();
	return true;
}

bool Game::wrongAnswer()
{
	Joueur* joueur = _joueurs.currentPlayer();

	std::cout << "Question was incorrectly answered" << std::endl;
	std::cout << joueur->_nom + " was sent to the penalty box" << std::endl;

	joueur->setPenalty(true);
	joueur->increaseTimeInPenalty();
	joueur->increaseInPrisonSince();
	joueur->loseWinstreak();

	std::cout <<"No more Winstreak"<< std::endl;
	std::cout<< "You are in the penalty box, you must choose the next theme : "<<std::endl;
	Categories categorie = Categories::Null;
	std::string val;
	do{
		std::cin>>val;
		categorie = getCategorieFromString(val);
	}while(categorie == Categories::Null);
	
	ChangeNextCategory(categorie);
	
	_joueurs.incrementCurrentPlayer();

	return true;
}

void Game::ChangeGoldNeededToWin(int gold)
{
	_goldNeededToWin = gold;
}

bool Game::useJoker()
{
	Joueur* joueur = _joueurs.currentPlayer();
	joueur->useJoker();

	std::cout << " a joker has been used" << std::endl;
	return true;

}
bool Game::getCurrentPlayerJoker()
{
	if(_joueurs.currentPlayer()->hasJoker())
	{
		std::cout<<"JOKER AVAILABLE"<<std::endl;
	}
	return _joueurs.currentPlayer()->hasJoker();
}

Joueur* Game::getCurrentPlayer()
{
	return _joueurs.currentPlayer();
}

void Game::reset()
{
	Game();
	_joueurs.reset();
}